let fill = ["pi&#228;", "&#154;ilm&#228;", "no'kka", "tukat", "&#154;uu", "korva", "ott&#154;a", "hampahat", "kakla", 
    "vartalo", "jalka", "varva&#154;", "k&#228;si", "&#154;ormi", "va&#269;&#269;a", "takapuoli", "&#154;elk&#228;", "polvi",
    "url('mg2 images/pia.jpg')", "url('mg2 images/silma.jpg')", "url('mg2 images/nokka.jpg')", "url('mg2 images/tukat.jpg')", "url('mg2 images/suu.jpg')", "url('mg2 images/korva.jpg')", "url('mg2 images/ottsa.jpg')", "url('mg2 images/hampahat.jpg')", "url('mg2 images/kakla.jpg')",
    "url('mg2 images/vartalo.jpg')", "url('mg2 images/jalka.jpg')", "url('mg2 images/varvas.jpg')", "url('mg2 images/kasi.jpg')", "url('mg2 images/sormi.jpg')", "url('mg2 images/vacca.jpg')", "url('mg2 images/takapuoli.jpg')", "url('mg2 images/selka.jpg')", "url('mg2 images/polvi.jpg')"];

let asciiKey = {
    piä: "pia",
    šilmä: "silma",
    "no'kka": "nokka",
    tukat: "tukat",
    šuu: "suu",
    korva: "korva",
    ottša: "ottsa",
    hampahat: "hampahat", 
    kakla: "kakla", 
    vartalo: "vartalo", 
    jalka: "jalka",
    varvaš: "varvas",
    käsi: "kasi",
    šormi: "sormi",
    vačča: "vacca",
    takapuoli: "takapuoli",
    šelkä: "selka",
    polvi: "polvi"
}

const spaces = document.getElementsByClassName("tile");
const spacesArray = Array.from(spaces);
const backs = document.getElementsByClassName("backSide");
const backsArray = Array.from(backs);
const fronts = document.getElementsByClassName("frontSide");
const frontsArray = Array.from(fronts);

const endingModal = document.getElementsByClassName("ending")[0];
const againButton = document.getElementById("playAgain");
const notAgainButton = document.getElementById("dontPlayAgain");
againButton.addEventListener("click", () => {
    document.location.reload();
})
notAgainButton.addEventListener("click", () => {
    endingModal.style.display = "none";
})


//fill spaces when loading
window.addEventListener("load", getRandom);

function getRandom() {
    for (let i = 0; i < spacesArray.length; i++){
        let length = fill.length;
        const x = fill[Math.floor(Math.random() * length)];
        if (x.slice(0, 3) === "url"){
            spacesArray[i].children[1].style.backgroundImage = x;
        } else {
            spacesArray[i].children[1].innerHTML = x;
        }
        const index = fill.indexOf(x);
        const usedItem = fill.splice(index, 1);
    }
}

//flip card when card clicked 
for (let i = 0; i < spaces.length; i++){
    spaces[i].addEventListener("click", flipCard);
}

//click card to flip; maximum two;
function flipCard(){
    const backSide = this.children[0];
    const frontSide = this.children[1];
    const maxTwo = backsArray.filter(function(el){
        return el.className == "backSide animation1";
    })
    if (maxTwo.length < 2){
        if (backSide.className === "backSide"){
            backSide.className  += " animation1";
            frontSide.className += " animation2";
        }
    }

    const flipped = frontsArray.filter(function(el){
        return el.className === "frontSide animation2";
    });
    const flipped1 = document.getElementById(flipped[0].id);
    const flipped2 = document.getElementById(flipped[1].id);
    const turned = spacesArray.filter(function(el){
        return el.children[0].className === "backSide animation1";
    });
    
        if (flipped1.innerHTML && flipped2.innerHTML){
            for (let i = 0; i < turned.length; i++) {
                setTimeout(() => (turned[i].children[0].className = "backSide hidden"), 500);
                setTimeout(() => (turned[i].children[1].className = "frontSide seen"), 500);
                setTimeout(() => (turned[i].children[0].className += " animation2"), 1000);
                setTimeout(() => (turned[i].children[1].className += " animation1"), 1000);
                setTimeout(() => (turned[i].children[0].className = "backSide"), 1500);
                setTimeout(() => (turned[i].children[1].className = "frontSide"), 1500);
            }   
        } else if (flipped1.style.backgroundImage && flipped2.style.backgroundImage){
            for (let i = 0; i < turned.length; i++) {
                setTimeout(() => (turned[i].children[0].className = "backSide hidden"), 500);
                setTimeout(() => (turned[i].children[1].className = "frontSide seen"), 500);
                setTimeout(() => (turned[i].children[0].className += " animation2"), 1000);
                setTimeout(() => (turned[i].children[1].className += " animation1"), 1000);
                setTimeout(() => (turned[i].children[0].className = "backSide"), 1500);
                setTimeout(() => (turned[i].children[1].className = "frontSide"), 1500);
            }  
        } else {
            if (flipped1.innerHTML) {
                let asciiFree = asciiKey[flipped1.innerHTML];
                if (asciiFree === flipped2.style.backgroundImage.slice(16, -6)){
                    for (let i = 0; i < turned.length; i++) {
                        setTimeout(() => (turned[i].children[1].className = "frontSide correct"), 1000);
                        setTimeout(() => (turned[i].children[1].className = "frontSide correctOut"), 2000);
                        setTimeout(() => (turned[i].children[0].className = "backSide hidden"), 2000);
                    }
                    setTimeout(() => (checkBacksides()), 2500);
                } else {
                    for (let i = 0; i < turned.length; i++) {
                        setTimeout(() => (turned[i].children[0].className = "backSide hidden"), 500);
                        setTimeout(() => (turned[i].children[1].className = "frontSide seen"), 500);
                        setTimeout(() => (turned[i].children[0].className += " animation2"), 1000);
                        setTimeout(() => (turned[i].children[1].className += " animation1"), 1000);
                        setTimeout(() => (turned[i].children[0].className = "backSide"), 1500);
                        setTimeout(() => (turned[i].children[1].className = "frontSide"), 1500);
                    }
                }
            } else if (flipped2.innerHTML) {
                let asciiFree = asciiKey[flipped2.innerHTML];
                if (flipped1.style.backgroundImage.slice(16, -6) === asciiFree) {
                    for (let i = 0; i < turned.length; i++) {
                        setTimeout(() => (turned[i].children[1].className = "frontSide correct"), 1000);
                        setTimeout(() => (turned[i].children[1].className = "frontSide correctOut"), 2000);
                        setTimeout(() => (turned[i].children[0].className = "backSide hidden"), 2000);
                    }
                    setTimeout(() => (checkBacksides()), 2500);
                } else {
                    for (let i = 0; i < turned.length; i++) {
                        setTimeout(() => (turned[i].children[0].className = "backSide hidden"), 500);
                        setTimeout(() => (turned[i].children[1].className = "frontSide seen"), 500);
                        setTimeout(() => (turned[i].children[0].className += " animation2"), 1000);
                        setTimeout(() => (turned[i].children[1].className += " animation1"), 1000);
                        setTimeout(() => (turned[i].children[0].className = "backSide"), 1500);
                        setTimeout(() => (turned[i].children[1].className = "frontSide"), 1500);
                    }
                }
            }

        }

}

function checkBacksides(){
    const backsidesHidden = document.getElementsByClassName("backSide hidden").length;
        if (backsidesHidden === 36){
           endingModal.style.display = "block";
        }

}
