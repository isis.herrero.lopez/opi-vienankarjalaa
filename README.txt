JavaScript-based games for learning Viena-Karjala language.
Project leader: Leena Arhippainen, University of Oulu. 
Date: July-October 2019.

Crossword no. 3:
it is designed to be reused. 
It's enough to change the first 35 lines of the file cw3.js.

Memory Game no.3:
It is an extension of memory game no.2: 
when a pair of images and noun are found, 
the noun must be used in a sentence.
Like crossword no.3, it is designed to be reused.
It's enough to change the first 53 lines of the file mg3.js