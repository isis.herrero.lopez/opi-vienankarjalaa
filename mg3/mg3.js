let fill = ["pi&#228;", "&#154;ilm&#228;", "no'kka", "tukat", "&#154;uu", "korva", "ott&#154;a", "hampahat", "kakla", 
    "vartalo", "jalka", "varva&#154;", "k&#228;si", "&#154;ormi", "va&#269;&#269;a", "takapuoli", "&#154;elk&#228;", "polvi",
    "url('mg3 images/pia.jpg')", "url('mg3 images/silma.jpg')", "url('mg3 images/nokka.jpg')", "url('mg3 images/tukat.jpg')", "url('mg3 images/suu.jpg')", "url('mg3 images/korva.jpg')", "url('mg3 images/ottsa.jpg')", "url('mg3 images/hampahat.jpg')", "url('mg3 images/kakla.jpg')",
    "url('mg3 images/vartalo.jpg')", "url('mg3 images/jalka.jpg')", "url('mg3 images/varvas.jpg')", "url('mg3 images/kasi.jpg')", "url('mg3 images/sormi.jpg')", "url('mg3 images/vacca.jpg')", "url('mg3 images/takapuoli.jpg')", "url('mg3 images/selka.jpg')", "url('mg3 images/polvi.jpg')"];

    let asciiKey = {
        piä: "pia",
        šilmä: "silma",
        "no'kka": "nokka",
        tukat: "tukat",
        šuu: "suu",
        korva: "korva",
        ottša: "ottsa",
        hampahat: "hampahat", 
        kakla: "kakla", 
        vartalo: "vartalo", 
        jalka: "jalka",
        varvaš: "varvas",
        käsi: "kasi",
        šormi: "sormi",
        vačča: "vacca",
        takapuoli: "takapuoli",
        šelkä: "selka",
        polvi: "polvi"
    }

const spaces = document.getElementsByClassName("tile");
const spacesArray = Array.from(spaces);
const backs = document.getElementsByClassName("backSide");
const backsArray = Array.from(backs);
const fronts = document.getElementsByClassName("frontSide");
const frontsArray = Array.from(fronts);

const sentences = {
    pia: 'Miula on hattu <input type="text" name="piäššä">.',
    silma: 'Hänellä on kaunehet <input type="text" name="šilmät">.',
    nokka: 'Hiän tuaton <input type="text" name="no&#39;kka"> on šuuri.',
    tukat: 'Ämmöllä on kihara valkie <input type="text" nme="tukka">.',
    suu: 'Pikkaraini lapši kuatu ta šatatti <input type="text" name="šuutah"> . Pitäis käyvä hammašliäkärissä.',
    korva: 'Miula <input type="text" name="korvua"> pistäy lujah.',
    ottsa: 'Šiula on hiki <input type="text" name="ottšašša">.',
    hampahat: 'Miula <input type="text" name="hammahašta"> porottau.',
    kakla: 'Tuatolla on ruskie kravatti <input type="text" name="kaklašša">.',
    vartalo: 'Työ opaššutta <input type="text" name="vartalon">ošie vienankarjalakši.',
    jalka: 'Šukat ollah <input type="text" name="jalašša">.',
    varvas: 'Šiula on kymmennen <input type="text" name="varvašta">.',
    kasi: 'Hiän katkasi <input type="text" name="käteh">.',
    sormi: 'Sormus on <input type="text" name="šormešša">.',
    vacca: 'Šiula <input type="text" name="vaččua"> koškou. Kuiva mussikka on hyvä vaččatautih.',
    takapuoli: 'Miula on takapuoli kipienä. Ampujaini pisti miuta <input type="text" name="takapuoleh">.',
    selka: '<input type="text" name="Šelkyä"> kivistäy.',
    polvi: 'Hiän kuatu ta šatatti <input type="text" name="polveh">.'
}

let button;
const sentenceSection = document.getElementsByClassName("sentence")[0];

const endingModal = document.getElementsByClassName("ending")[0];
const againButton = document.getElementById("playAgain");
const notAgainButton = document.getElementById("dontPlayAgain");
againButton.addEventListener("click", () => {
    document.location.reload();
})
notAgainButton.addEventListener("click", () => {
    endingModal.style.display = "none";
})


//fill spaces when loading
window.addEventListener("load", getRandom);

function getRandom() {
    for (let i = 0; i < spacesArray.length; i++){
        let length = fill.length;
        const x = fill[Math.floor(Math.random() * length)];
        if (x.slice(0, 3) === "url"){
            spacesArray[i].children[1].style.backgroundImage = x;
        } else {
            spacesArray[i].children[1].innerHTML = x;
        }
        const index = fill.indexOf(x);
        const usedItem = fill.splice(index, 1);
    }
}


//flip card when card clicked 
for (let i = 0; i < spaces.length; i++){
    spaces[i].addEventListener("click", flipCard);
}

//click card to flip; maximum two;
function flipCard(){
    const backSide = this.children[0];
    const frontSide = this.children[1];
    const maxTwo = backsArray.filter(function(el){
        return el.className == "backSide animation1";
    });
    if (sentenceSection.className === "sentence") {
        if (maxTwo.length < 2){
            if (backSide.className === "backSide"){
                backSide.className  += " animation1";
                frontSide.className += " animation2";
            }
        }
    }

    const flipped = frontsArray.filter(function(el){
        return el.className === "frontSide animation2";
    });
    const flipped1 = document.getElementById(flipped[0].id);
    const flipped2 = document.getElementById(flipped[1].id);
    const turned = spacesArray.filter(function(el){
        return el.children[0].className === "backSide animation1";
    });
    
    if (flipped1.innerHTML && flipped2.innerHTML){
        for (let i = 0; i < turned.length; i++) {
            setTimeout(() => (turned[i].children[0].className = "backSide hidden"), 500);
            setTimeout(() => (turned[i].children[1].className = "frontSide seen"), 500);
            setTimeout(() => (turned[i].children[0].className += " animation2"), 1000);
            setTimeout(() => (turned[i].children[1].className += " animation1"), 1000);       
           setTimeout(() => (turned[i].children[0].className = "backSide"), 1500);
            setTimeout(() => (turned[i].children[1].className = "frontSide"), 1500);
        }   
    } else if (flipped1.style.backgroundImage && flipped2.style.backgroundImage){
        for (let i = 0; i < turned.length; i++) {
            setTimeout(() => (turned[i].children[0].className = "backSide hidden"), 500);
            setTimeout(() => (turned[i].children[1].className = "frontSide seen"), 500);
            setTimeout(() => (turned[i].children[0].className += " animation2"), 1000);
            setTimeout(() => (turned[i].children[1].className += " animation1"), 1000);
            setTimeout(() => (turned[i].children[0].className = "backSide"), 1500);
            setTimeout(() => (turned[i].children[1].className = "frontSide"), 1500);
        }  
    } else {
        if (flipped1.innerHTML) {
            let asciiFree = asciiKey[flipped1.innerHTML];
            if (asciiFree === flipped2.style.backgroundImage.slice(16, -6)){
                for (let i = 0; i < turned.length; i++) {
                    setTimeout(() => (turned[i].children[1].className = "frontSide correct"), 1000);
                    setTimeout(() => (turned[i].children[1].className = "frontSide correctOut"), 2000);
                    setTimeout(() => (turned[i].children[0].className = "backSide hidden"), 2000);
                }     
                setTimeout(() => (sentenceSection.style.display = "block"), 3000);
                const key = asciiFree;
                const wordSentence = sentences[key];
                sentenceSection.children[13].innerHTML = `${wordSentence}`;
                button = document.getElementById("button");
//print sentence when check-button clicked
button.addEventListener("click", revise);            
            } else {
                for (let i = 0; i < turned.length; i++) {
                    setTimeout(() => (turned[i].children[0].className = "backSide hidden"), 500);
                    setTimeout(() => (turned[i].children[1].className = "frontSide seen"), 500);
                    setTimeout(() => (turned[i].children[0].className += " animation2"), 1000);    
                    setTimeout(() => (turned[i].children[1].className += " animation1"), 1000);
                    setTimeout(() => (turned[i].children[0].className = "backSide"), 1500);
                    setTimeout(() => (turned[i].children[1].className = "frontSide"), 1500);
                }
            }
        } else if (flipped2.innerHTML) {
            let asciiFree = asciiKey[flipped2.innerHTML];
            if (flipped1.style.backgroundImage.slice(16, -6) === asciiFree) {
                for (let i = 0; i < turned.length; i++) {
                    setTimeout(() => (turned[i].children[1].className = "frontSide correct"), 1000);
                    setTimeout(() => (turned[i].children[1].className = "frontSide correctOut"), 3000);
                    setTimeout(() => (turned[i].children[0].className = "backSide hidden"), 3000);
                }
                setTimeout(() => (sentenceSection.style.display = "block"), 3000);
                const key = asciiFree;
                const wordSentence = sentences[key];
                sentenceSection.children[13].innerHTML = `${wordSentence}`;
                button = document.getElementById("button");

                button.addEventListener("click", revise);
            } else {
                for (let i = 0; i < turned.length; i++) {
                    setTimeout(() => (turned[i].children[0].className = "backSide hidden"), 500);
                    setTimeout(() => (turned[i].children[1].className = "frontSide seen"), 500);
                    setTimeout(() => (turned[i].children[0].className += " animation2"), 1000);
                    setTimeout(() => (turned[i].children[1].className += " animation1"), 1000);
                    setTimeout(() => (turned[i].children[0].className = "backSide"), 1500);
                    setTimeout(() => (turned[i].children[1].className = "frontSide"), 1500);
                }
            }
        }
    }
}

function addLetter(id){
    const letter = id;
    let input = document.getElementsByTagName("INPUT")[0];
    input.value += letter;
    input.focus();
}

let answer, rightAnswer;

function revise(){
    answer = sentenceSection.children[13].children[0].value;
    rightAnswer = sentenceSection.children[13].children[0].name;
    if (answer === rightAnswer){
        const finished = frontsArray.every(function(el){
            return el.className === "frontSide correctOut";
        });
        if (finished === true){
            sentenceSection.innerHTML = `<h2>Onnea! Kaikki lauseet ovat oikein.</h2>`
            setTimeout(() => (endingModal.style.display = "block"), 500);
        } else {
            sentenceSection.innerHTML = `<h2>Onnea! Jatka pelaamista.</h2>`
            setTimeout(() => (sentenceSection.style.display = "none"), 1000);
            setTimeout(() => (sentenceSection.innerHTML = `<h3>Nyt käytä tätä sanaa seuraavassa lauseessa:</h3>
            <button class="charBtn" id="&#138;" onclick="addLetter('&#138;')">&#138;</button>
            <button class="charBtn" id="&#154;" onclick="addLetter('&#154;')">&#154;</button>
            <button class="charBtn" id="&#268;" onclick="addLetter('&#268;')">&#268;</button>
            <button class="charBtn" id="&#269;" onclick="addLetter('&#269;')">&#269;</button>
            <button class="charBtn" id="&#381;" onclick="addLetter('&#381;')">&#381;</button>
            <button class="charBtn" id="&#382;" onclick="addLetter('&#382;')">&#382;</button>
            <button class="charBtn" id="&#196;" onclick="addLetter('&#196;')">&#196;</button>
            <button class="charBtn" id="&#228;" onclick="addLetter('&#228;')">&#228;</button>
            <button class="charBtn" id="&#214;" onclick="addLetter('&#214;')">&#214;</button>
            <button class="charBtn" id="&#246;" onclick="addLetter('&#246;')">&#246;</button>
            <button class="charBtn" id="&#39;" onclick="addLetter('&#39;')">&#39;</button>
            </br>
            <p></p> 
            <input id="button" type="submit" value="Tarkista">`), 1200);
        }
    } else {
        if (sentenceSection.children[0].outerHTML === "<h2>Väärä vastaus. Yritä uudelleen.</h2>"){
            sentenceSection.innerHTML += `<input id="sentenceButton" type="submit" onclick="getSentence()" value="Näytä oikea vastaus">`;
        } else {
            sentenceSection.children[0].outerHTML = `<h2>Väärä vastaus. Yritä uudelleen.</h2>`;
        }
    }
}

function getSentence(){
    let inputIndexA = sentenceSection.children[13].innerHTML.indexOf("<");
    let inputIndexB = sentenceSection.children[13].innerHTML.indexOf(">") + 1;
    let beforeInput = sentenceSection.children[13].innerHTML.substring(0, inputIndexA);
    let afterInput = sentenceSection.children[13].innerHTML.substring(inputIndexB, sentenceSection.children[13].length);
    let inputAnswer = sentenceSection.children[13].children[0].name;

    let finalSentence = beforeInput + inputAnswer + afterInput;
    sentenceSection.innerHTML += `<h4>Oikea vastaus:</h4><p>${finalSentence}</p>`;

    setTimeout(() => (sentenceSection.innerHTML += `<h4>Nyt, jatka pelaamista.</h4>`), 3000);
    setTimeout(() => (sentenceSection.style.display = "none"), 5000);
    setTimeout(() => (sentenceSection.removeChild(sentenceSection.children[19])), 5500);
    setTimeout(() => (sentenceSection.removeChild(sentenceSection.children[18])), 5500);
    setTimeout(() => (sentenceSection.removeChild(sentenceSection.children[17])), 5500);
    setTimeout(() => (sentenceSection.removeChild(sentenceSection.children[16])), 5500);
    setTimeout(() => (sentenceSection.removeChild(sentenceSection.children[15])), 5500);
    setTimeout(() => (sentenceSection.children[0].innerHTML = `<h3>Nyt käytä tätä sanaa seuraavassa lauseessa:</h3>`), 5500);

}