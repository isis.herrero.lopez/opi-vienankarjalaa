const exercise = document.getElementsByClassName("exercise");
const wordSection = document.getElementById("wordSection");
const words = document.getElementsByClassName("wordBox");
const wordRow = document.getElementsByClassName("wordRow");
const boxImages = document.getElementsByClassName("box image");
const letterBoxes = document.getElementsByClassName("box");
const noLetterBoxes = document.getElementsByClassName("noBox");


const wordBoxes = wordSection.children;
//wordBoxes is not an array; it's a HTML Collection -->array
const wordsArray = Array.from(wordBoxes);

//answers:
const rightAnswers = {
    answer1: document.getElementById("word4").innerHTML,
    answer2: document.getElementById("word2").innerHTML,
    answer3: document.getElementById("word3").innerHTML,
    answer4: document.getElementById("word7").innerHTML,
    answer5: document.getElementById("word1").innerHTML,
    answer6: document.getElementById("word6").innerHTML,
    answer7: document.getElementById("word8").innerHTML,
    answer8: document.getElementById("word5").innerHTML,
    answer9: document.getElementById("word9").innerHTML,
}

const endingModal = document.getElementsByClassName("ending")[0];
const againButton = document.getElementById("playAgain");
const notAgainButton = document.getElementById("dontPlayAgain");
againButton.addEventListener("click", () => {
    document.location.reload();
})

notAgainButton.addEventListener("click", () => {
    endingModal.style.display = "none";
})

let windowWidth = window.innerWidth
|| document.documentElement.clientWidth
|| document.body.clientWidth;

let wordSelectionId, rowSelection;
let wordSelection = "";

//fill each first Box with an image
window.addEventListener("load", fillImages);

function fillImages() {
    for (let i = 0; i < boxImages.length; i++) {
        boxImages[i].style.backgroundImage = "url('cw1 images/image" + (i + 1) + ".jpg')";
        boxImages[i].style.backgroundRepeat = "no-repeat";
        boxImages[i].style.backgroundPosition = "center";
        boxImages[i].style.backgroundSize = "contain";
        boxImages[i].style.borderLeft = "1px solid black";
    }
    for (let i = 0; i < letterBoxes.length; i++) {
        const id = Number.parseInt(letterBoxes[i].id.slice(1), 10);
        const lower = "b" + (id + 10);
        if (letterBoxes[i].style.borderBottom === "" && document.getElementById(lower).className === "noBox") {
            letterBoxes[i].style.borderBottom = "1px solid black";
        }
    }
}

//word listeners - to catch them; wiht loop, bcs of being a ClassName
if (windowWidth <= 1280){
    for (let i = 0; i < words.length; i++) {
        words[i].addEventListener("click", selectWord);
    }
} else {
    for (let i = 0; i < words.length; i++) {
        words[i].addEventListener("dragstart", dragStart);
        words[i].addEventListener("dragend", dragEnd);
    }
}

//letterBoxes listeners
if (windowWidth <= 1280){
    //nothing
} else {
    for (let i = 0; i < letterBoxes.length; i++) {
        letterBoxes[i].addEventListener("dragover", dragOver);
        letterBoxes[i].addEventListener("dragenter", dragEnter);
        letterBoxes[i].addEventListener("dragleave", dragLeave);
        letterBoxes[i].addEventListener("drop", dragDrop);
    }
}

//listener for other areas
if (windowWidth <= 1280){
    //nothing;
} else {
    exercise[0].addEventListener("drop", dragDrop);
    exercise[0].addEventListener("dragover", dragOver);
    wordSection.addEventListener("drop", dragDrop);
    wordSection.addEventListener("dragover", dragOver);
    for (let i = 0; i < wordRow.length; i++) {
        wordRow[i].addEventListener("drop", dragDrop);
        wordRow[i].addEventListener("dragover", dragOver);
    }

    for (let i = 0; i < noLetterBoxes.length; i++) {
        noLetterBoxes[i].addEventListener("drop", dragDrop);
        noLetterBoxes[i].addEventListener("dragover", dragOver);
    }
}

//dragging functions
function dragStart(e) {
    //we use this. for indicating the specific word being dragged
    e.dataTransfer.setData("Text", this.innerHTML);
    e.dataTransfer.setData("Id", this.id);
    this.className += " hold";
    setTimeout(() => (this.className = "invisible"), 0);
    setTimeout(() => (this.innerHTML = ""), 0);
}

function dragEnd() {
    const allInvisible = wordsArray.every(function (el) {
        return el.className === "invisible";
    });
    //when ended, print message to section #;
    if (allInvisible === true) {
        //done first part;

        for (let i = 15; i < 105; i += 10) {
            document.getElementById("b" + i).style.borderTop = "2px solid black";
            document.getElementById("b" + i).style.borderLeft = "2px solid black";
            document.getElementById("b" + i).style.borderRight = "2px solid black";
        }
        document.getElementById("b95").style.borderBottom = "2px solid black";

        document.getElementById("wordSection").id += "B";
        document.getElementById("wordSectionB").innerHTML = `<div>
                                                                <p>Onnittelut!.</br>
                                                                Lue pystysuorassa oleva korostettu sana ja valitse numero johon se viittaa.</p>
                                                                <div class="newImages" id="newImages"> 
                                                                    <div class="image" id="numberA"></div> 
                                                                    <div class="image" id="numberB"></div> 
                                                                    <div class="image" id="numberC"></div> 
                                                                </div>  
                                                            </div>`

        const newImages = document.getElementById("newImages").children;
        for (let i = 0; i < newImages.length; i++) {
            newImages[i].addEventListener("click", checkNumber);
        }
    }
}


function checkNumber() {
    if (this.id === "numberB") {
        this.style.border = "2px solid #17a2b8";
        document.getElementById("wordSectionB").children[0].innerHTML += `<p>Huikeaa! Onnittelut taas!</p>`;

        setTimeout(() => (endingModal.style.display = "block"), 500);

    } else {
        this.style.border = "2px solid red";
        document.getElementById("wordSectionB").children[0].innerHTML += `<p>Ei mennyt oikein. Yrit&#228; uudestaan.</p>`;

        const newImages = document.getElementById("newImages").children;
        if (this.id === "numberA") {
            for (let i = 1; i < newImages.length; i++) {
                newImages[i].addEventListener("click", checkNewNumber);
            }
        } else if (this.id === "numberC") {
            for (let i = 0; i < newImages.length - 1; i++) {
                newImages[i].addEventListener("click", checkNewNumber);
            }
        }
    }
}

function checkNewNumber() {
    if (this.id === "numberB") {
        this.style.border = "2px solid #17a2b8";
        document.getElementById("wordSectionB").children[0].innerHTML += `<div><p>Huikeaa, onnittelut taas!</p></div>`;

       setTimeout(() => (endingModal.style.display = "block"), 500);

    } else {
        this.style.border = "2px solid red";
        document.getElementById("wordSectionB").children[0].innerHTML += `<p>Ei ole oikein. Oikea vastaus on korostettu sinisell&#228; v&#228;rill&#228;..</p>`;
        document.getElementById("numberB").style.border = "2px solid #17a2b8";

       setTimeout(() => (endingModal.style.display = "block"), 500);

    }
}


function dragOver(e) {
    e.preventDefault();
    if (this.className === "box" || this.className === "box hovered") {
        const thisBox = document.getElementById(this.id);
        const parent = thisBox.parentNode;
        for (let i = 0; i < parent.children.length; i++) {
            if (parent.children[i].className === "box") {
                parent.children[i].className += " hovered";
            }
        }
    }
}

function dragEnter(e) {
    e.preventDefault();
    if (this.className === "box") {
        const thisBox = document.getElementById(this.id);
        const parent = thisBox.parentNode;
        for (let i = 0; i < parent.children.length; i++) {
            if (parent.children[i].className === "box") {
                parent.children[i].className += " hovered";
            }
        }
    }
}

function dragLeave() {
    if (this.className === "box hovered") {
        const thisBox = document.getElementById(this.id);
        const parent = thisBox.parentNode;
        for (let i = 0; i < parent.children.length; i++) {
            if (parent.children[i].className === "box hovered") {
                parent.children[i].className = "box";
            }
        }
    }
}

function dragDrop(e) {
    e.preventDefault();
    e.stopPropagation();
    //dataTransfer.getData only available when dropping (security reasons)

    const selectedWord = e.dataTransfer.getData("Text");
    const wordId = e.dataTransfer.getData("Id");

    if (this.className === "box hovered") {
        const thisBox = document.getElementById(this.id);
        const parent = thisBox.parentNode;
        const rightAnswer = rightAnswers[parent.id];

        if (selectedWord === rightAnswer) {
            document.getElementById(wordId).className = "invisible";
            let imagePlace;
            for (let i = 0; i < parent.children.length; i++) {
                if (parent.children[i].className === "box image") {
                    imagePlace = Number.parseInt(parent.children[i].id.slice(2), 10);
                } else if (parent.children[i].className === "box hovered") {
                    const letterIndex = i - imagePlace;
                    parent.children[i].innerHTML = selectedWord[letterIndex];
                    parent.children[i].className = "box correct";
                    setTimeout(() => (parent.children[i].className = "box"), 500);
                }
            }
        } else {
            //if it's incorrect, return word to the upper panel
            for (let i = 0; i < parent.children.length; i++) {
                if (parent.children[i].className === "box hovered") {
                    parent.children[i].className = "box incorrect";
                    setTimeout(() => (parent.children[i].className = "box"), 500);
                }
            }
            document.getElementById(wordId).className = "wordBox incorrect";
            document.getElementById(wordId).innerHTML = `${selectedWord}`;
            setTimeout(() => (document.getElementById(wordId).className = "wordBox"), 500);
        }
    } else if (this.className !== "box hovered") {
        document.getElementById(wordId).className = "wordBox";
        document.getElementById(wordId).innerHTML = `${selectedWord}`;
    }
}

//clicking functions

function selectWord(){
    if (wordSelection === ""){
        wordSelectionId = this.id;
        wordSelection = this.innerHTML;
        this.className += " hold";
    } else {
        document.getElementById(wordSelectionId).className = "wordBox";
        wordSelectionId = this.id;
        wordSelection = this.innerHTML;
        this.className += " hold";
    }
    rowListeners();

}

function rowListeners(){
    if (wordSelection != ""){
        for (let i = 0; i < wordRow.length; i++) {
            wordRow[i].addEventListener("mouseover", mouseOver);
            wordRow[i].addEventListener("mouseenter", mouseEnter);
            wordRow[i].addEventListener("mouseleave", mouseLeave);
            wordRow[i].addEventListener("click", dropWord);
        }
    }
}

function mouseOver(){
    rowSelection = this.id;
    for (let i = 0; i < document.getElementById(rowSelection).children.length; i++){
        if (document.getElementById(rowSelection).children[i].className === "box"){
            document.getElementById(rowSelection).children[i].className += " hovered";
        }
    }    
}

function mouseEnter(){
    rowSelection = this.id;
    for (let i = 0; i < document.getElementById(rowSelection).children.length; i++){
        if (document.getElementById(rowSelection).children[i].className === "box"){
            document.getElementById(rowSelection).children[i].className += " hovered";
        }
    }
}

function mouseLeave(){
    rowSelection = this.id;
    for (let i = 0; i < document.getElementById(rowSelection).children.length; i++){
        if (document.getElementById(rowSelection).children[i].className === "box hovered"){
            document.getElementById(rowSelection).children[i].className = "box";
        }
    }
}

function dropWord(){
    rowSelection = this.id;
    let goodAnswer = rightAnswers[rowSelection];
    if (wordSelection === goodAnswer){
        document.getElementById(wordSelectionId).className = "invisible";
        let imagePlace;
        for (let i = 0; i < document.getElementById(rowSelection).children.length; i++){
            if (document.getElementById(rowSelection).children[i].className === "box image"){
                imagePlace = Number.parseInt(document.getElementById(rowSelection).children[i].id.slice(2), 10);
            } else if (document.getElementById(rowSelection).children[i].className === "box hovered"){
                const letterIndex = i - imagePlace;
                document.getElementById(rowSelection).children[i].innerHTML = wordSelection[letterIndex];
                document.getElementById(rowSelection).children[i].className = "box correct";
                setTimeout(() => (document.getElementById(rowSelection).children[i].className = "box"), 500);
            }
        }
    } else {
//incorrect
        for (let i = 0; i < document.getElementById(rowSelection).children.length; i++){
            if (document.getElementById(rowSelection).children[i].className === "box hovered"){
                document.getElementById(rowSelection).children[i].className = "box incorrect";
                setTimeout(() => (document.getElementById(rowSelection).children[i].className = "box"), 500);
                setTimeout(() => (document.getElementById(wordSelectionId).className = "wordBox"), 500);
            }
        }
    }

    for (let i = 0; i < wordRow.length; i++) {
        wordRow[i].removeEventListener("mouseover", mouseOver);
        wordRow[i].removeEventListener("mouseenter", mouseEnter);
        wordRow[i].removeEventListener("mouseleave", mouseLeave);
        wordRow[i].removeEventListener("click", dropWord);
    }

    wordSelection = "";

    dragEnd();

}