let imageId, box;
let initial, boxNumber;
let lastBox;
let selected;

const boxesFull = document.getElementsByClassName("letterBox full");

const inputs = document.getElementsByTagName("input");
for (let i = 0; i < inputs.length; i++){
    inputs[i].setAttribute("disabled", true);
}

const endingModal = document.getElementsByClassName("ending")[0];
const againButton = document.getElementById("playAgain");
const notAgainButton = document.getElementById("dontPlayAgain");
againButton.addEventListener("click", () => {
    document.location.reload();
})
notAgainButton.addEventListener("click", () => {
    endingModal.style.display = "none";
})

const imageBoxes = ["b104", "b109", "b308", "b406", "b508", "b602", "b612", "b807", "b1003", "b1102", "b1110", "b1305", "b1601"];
const answers = ["lehmä", "skokuna", "kana", "kišša", "koira", "jänis", "muurahaini", "varpuni", "heponi", "hukka", "kontie", "repo", "liipukkaini"];
const images = ["lehma", "skokuna", "kana", "kissa", "koira", "janis", "muurahaini", "varpuni", "heponi", "hukka", "kontie", "repo", "liipukkaini"];

window.addEventListener("load", fillImages);

function fillImages(){
    for (let i = 0; i < imageBoxes.length; i++){
        document.getElementById(imageBoxes[i]).style.backgroundImage = "url('cw2 images/" + images[i] + ".png')";
    }
}

const verticalOnes = ["b104", "b109", "b406", "b612", "b1003", "b1110", "b1305"];
for (let i = 0; i < imageBoxes.length; i++){
    document.getElementById(imageBoxes[i]).addEventListener("click", activateWriting);
}

let myThis, clientY;

function activateWriting(ev){
    if (this.className === "letterBox full" && document.getElementsByClassName("letterBox full selected").length === 0){
        this.className = "letterBox full selected";
        myThis = this;
        clientY = ev.clientY;

        imageLarger();

        imageId = this.id;
        box = Number.parseInt(imageId.slice(1), 10);
        if (verticalOnes.includes(imageId)){
            if (imageId.length === 4){
                lenght4V();
            } else if (imageId.length === 5){
                length5V();
            }
        } else {
            if (imageId.length === 4){
                length4H();
            } else if (imageId.length === 5){
                length5H();
            }
        }
    } else if (this.className === "letterBox full" && document.getElementsByClassName("letterBox full selected").length === 1){
        selected = document.getElementsByClassName("letterBox full selected")[0].id;
        box = Number.parseInt(selected.slice(1), 10);
        if (verticalOnes.includes(selected)){
            if (selected.length === 4){
                empty4V();

                this.className = "letterBox full selected";
                myThis = this;
                clientY = ev.clientY;
                imageLarger();

                imageId = this.id;
                box = Number.parseInt(imageId.slice(1), 10);
                if (verticalOnes.includes(imageId)){
                    if (imageId.length === 4){
                        lenght4V();
                    } else if (imageId.length === 5){
                        length5V();
                    }
                } else {
                    if (imageId.length === 4){
                        length4H();
                    } else if (imageId.length === 5){
                        length5H();
                    }
                }
            } else if (imageId.length === 5){
                empty5V();

                this.className = "letterBox full selected";
                myThis = this;
                clientY = ev.clientY;
                imageLarger();

                imageId = this.id;
                box = Number.parseInt(imageId.slice(1), 10);
                if (verticalOnes.includes(imageId)){
                    if (imageId.length === 4){
                        lenght4V();
                    } else if (imageId.length === 5){
                        length5V();
                    }
                } else {
                    if (imageId.length === 4){
                        length4H();
                    } else if (imageId.length === 5){
                        length5H();
                    }
                }
            }
        } else {
            if (selected.length === 4){
                empty4H();

                this.className = "letterBox full selected";
                myThis = this;
                clientY = ev.clientY;
                imageLarger();

                imageId = this.id;
                box = Number.parseInt(imageId.slice(1), 10);
                if (verticalOnes.includes(imageId)){
                    if (imageId.length === 4){
                        lenght4V();
                    } else if (imageId.length === 5){
                        length5V();
                   }
                } else {
                    if (imageId.length === 4){
                        length4H();
                    } else if (imageId.length === 5){
                        length5H();
                    }
                }
            } else if (selected.length === 5){
                empty5H();

                this.className = "letterBox full selected";
                myThis = this;
                clientY = ev.clientY;
                imageLarger();

                imageId = this.id;
                box = Number.parseInt(imageId.slice(1), 10);
                if (verticalOnes.includes(imageId)){
                    if (imageId.length === 4){
                        lenght4V();
                    } else if (imageId.length === 5){
                        length5V();
                    }
                } else {
                    if (imageId.length === 4){
                        length4H();
                    } else if (imageId.length === 5){
                        length5H();
                    }
                }
            }
        }
    } else if (this.className === "letterBox full done") {
        //nothing should happen; it's done
    }
}

function focusJumpV(){
    if (document.activeElement.value.length === document.activeElement.maxLength){
        boxNumber = Number.parseInt(document.activeElement.id.slice(1), 10);
        if (document.getElementById("b" + (boxNumber + 100) + "") == null || document.getElementById("b" + (boxNumber + 100) + "").tagName !== "INPUT"){
            lastBox = Number.parseInt(this.id.slice(1), 10);
            initial = Number.parseInt(this.name, 10);
            if (this.id === "b1612"){
                initial = 712;
            }
            checkWordV();
        } else if (document.getElementById("b" + (boxNumber + 100) + "").className === "letterBox full correct"){
            if (document.getElementById("b" + (boxNumber + 200) + "").className === "letterBox full"){
                document.getElementById("b" + (boxNumber + 200) + "").focus();
            } else {
                lastBox = boxNumber + 100;
                if (document.getElementById("b" + (boxNumber + 100) + "").name){
                    initial = Number.parseInt(document.getElementById("b" + (boxNumber + 100) + "").name);
                } else {
                    initial = 712;
                }
                checkWordV();
            }
        } else {
            if (document.getElementById("b" + (boxNumber + 100) + "").className === "letterBox full correct"){
                document.getElementById("b" + (boxNumber + 200) + "").focus();
            } else {
                document.getElementById("b" + (boxNumber + 100) + "").focus();
            }
        }
    }
}

function focusJumpH(){
    if (document.activeElement.value.length === document.activeElement.maxLength){
        boxNumber = Number.parseInt(document.activeElement.id.slice(1), 10);
        if (document.getElementById("b" + (boxNumber + 1) + "") == null || document.getElementById("b" + (boxNumber + 1) + "").tagName !== "INPUT"){
            lastBox = lastBox = Number.parseInt(this.id.slice(1), 10);
            initial = Number.parseInt(this.name, 10);
            if (this.id === "b1612"){
                initial = "1602";
            }
            checkWordH();
        } else if (document.getElementById("b" + (boxNumber + 1) + "").className === "letterBox full correct"){
            if (document.getElementById("b" + (boxNumber + 2) + "").className === "letterBox full"){
                document.getElementById("b" + (boxNumber + 2) + "").focus();
            } else {
                lastBox = boxNumber + 1;
                if (document.getElementById("b" + (boxNumber + 1) + "").name){
                    initial = Number.parseInt(document.getElementById("b" + (boxNumber + 1) + "").name);
                } else {
                    initial = 1602;
                }
                checkWordH();
            }
        } else {
            if (document.getElementById("b" + (boxNumber + 1) + "").className === "letterBox full correct"){
                document.getElementById("b" + (boxNumber + 2) + "").focus();
            } else {
                document.getElementById("b" + (boxNumber + 1) + "").focus();
            }
        }
    }
}

let activeBox;
function getPreviousActive(){
    activeBox = document.activeElement.id;
}
function addLetter(id){
    const letter = id;
    document.getElementById(activeBox).value += letter;
    const activeNumber = Number.parseInt(activeBox.slice(1), 10);

    if (document.getElementById("b" + (activeNumber + 100) + "").tagName !== "INPUT"){
        boxNumber = activeNumber;
        lastBox = activeNumber;
        if (document.getElementById("b" + boxNumber + "").name){
            initial = Number.parseInt(document.getElementById("b" + boxNumber + "").name);
        } else {
            initial = 712;
        }
        checkWordV();
    } else if (document.getElementById("b" + (activeNumber + 100) + "").tagName === "INPUT" && document.getElementById("b" + (activeNumber + 100) + "").hasAttribute("disabled") === false){
        document.getElementById("b" + (activeNumber + 100) + "").focus();
    } else if (document.getElementById("b" + (activeNumber + 1) + "").tagName !== "INPUT"){
        boxNumber = activeNumber;
        lastBox = activeNumber;
        if (document.getElementById("b" + boxNumber + "").name){
            initial = Number.parseInt(document.getElementById("b" + boxNumber + "").name);
        } else {
            initial = 1602;
        }
        checkWordH();
    }  else if (document.getElementById("b" + (activeNumber + 1) + "").tagName === "INPUT" && document.getElementById("b" + (activeNumber + 1) + "").hasAttribute("disabled") === false){
        document.getElementById("b" + (activeNumber + 1) + "").focus();
    }
}

function checkWordV(){
    let word = "";
    for (let i = initial; i <= lastBox; i = i + 100){
        word += document.getElementById("b" + i + "").value;
    }
    let index = imageBoxes.indexOf("b" + (initial - 100) + "");
    const rightAnswer = answers[index];
    if (word === rightAnswer || word.toLowerCase() === rightAnswer){
        for (let i = initial; i <= lastBox; i = i + 100){
            if (document.getElementById("b" + i + "").className !== "letterBox full correct"){
                document.getElementById("b" + i + "").className += " correct";
                document.getElementById("b" + i + "").setAttribute("disabled", true);
            } else {
                //already correct;
            }
        }
        document.getElementById("b" + (initial - 100) + "").className = "letterBox full done";
        let boxesFullDone = document.getElementsByClassName("letterBox full done").length;
        if (boxesFullDone === 14){
            setTimeout(() => (endingModal.style.display = "block"), 500);
        }

    } else {
        for (let i = initial; i <= lastBox; i = i + 100){
            if (document.getElementById("b" + i + "").className === "letterBox full correct"){
                document.getElementById("b" + i + "").className += " wrong";
                setTimeout(() => (document.getElementById("b" + i + "").className = "letterBox full correct"), 500);
            } else {
            document.getElementById("b" + i + "").className += " wrong";
            document.getElementById("b" + i + "").setAttribute("disabled", true);
            setTimeout(() => (document.getElementById("b" + i + "").className = "letterBox full"), 500);
            setTimeout(() => (document.getElementById("b" + i + "").value = ""), 500);
            }
        }
        document.getElementById("b" + (initial - 100) + "").className = "letterBox full";
    }
}

function checkWordH(){
    let word = "";
    for (let i = initial; i <= lastBox; i++){
        word += document.getElementById("b" + i + "").value;
    }
    let index = imageBoxes.indexOf("b" + (initial - 1) + "");
    const rightAnswer = answers[index];
    if (word === rightAnswer || word.toLowerCase() === rightAnswer) {
        for (let i = initial; i <= lastBox; i++){
            if (document.getElementById("b" + i + "").className !== "letterBox full correct"){
                document.getElementById("b" + i + "").className += " correct";
                document.getElementById("b" + i + "").setAttribute("disabled", true);
            } else {
                //already correct;
            }
        }
        document.getElementById("b" + (initial - 1) + "").className = "letterBox full done";
        let boxesFullDone = document.getElementsByClassName("letterBox full done").length;
        if (boxesFullDone === 14){
            setTimeout(() => (endingModal.style.display = "block"), 500);
        }

    } else {
        for (let i = initial; i <= lastBox; i++){
            if (document.getElementById("b" + i + "").className === "letterBox full correct"){
                document.getElementById("b" + i + "").className += " wrong";
                setTimeout(() => (document.getElementById("b" + i + "").className = "letterBox full correct"), 500);
            } else {
            document.getElementById("b" + i + "").className += " wrong";
            document.getElementById("b" + i + "").setAttribute("disabled", true);
            setTimeout(() => (document.getElementById("b" + i + "").className = "letterBox full"), 500);
            setTimeout(() => (document.getElementById("b" + i + "").value = ""), 500);
            }
        }
        document.getElementById("b" + (initial - 1) + "").className = "letterBox full";
    }
}