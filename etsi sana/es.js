const images = document.getElementsByClassName("image");
const imagesArray = Array.from(images);

const letters = document.getElementsByTagName("td");
const lettersArray = Array.from(letters);

let inital = "";
let noneSelected;
let manySelected;
let onlySelected;
let someCorrect;
let onlyCorrect;

let windowWidth;
let firstLetter = ""
let lastLetter = "";

const endingModal = document.getElementsByClassName("ending")[0];
const againButton = document.getElementById("playAgain");
const notAgainButton = document.getElementById("dontPlayAgain");
againButton.addEventListener("click", () => {
    document.location.reload();
})
notAgainButton.addEventListener("click", () => {
    endingModal.style.display = "none";
})

window.addEventListener("load", getWidth);

function getWidth(){
    windowWidth = window.innerWidth
    || document.documentElement.clientWidth
    || document.body.clientWidth;
}

//images listeners
for (let i = 0; i < images.length; i++){
    images[i].addEventListener("click", selectImage);
}

//select image
function selectImage(){
    const noImage = imagesArray.find(function(el){
        return el.className === "imgSelected";
    });
    const selected = lettersArray.filter(function(el){
        return el.className === "letterSelected" || el.className === "letterCorrectRed";
    });
    if (noImage === undefined) {
        if (this.className === "image") {
            this.className = "imgSelected";
        }
    } else {
        if (this.className === "imgSelected"){
            this.className = "image";
            for (let i = 0; i < selected.length; i++){
                if (selected[i].className === "letterSelected"){
                    document.getElementById(selected[i].id).className = "";
                } else if (selected[i].className === "letterCorrectRed"){
                    document.getElementById(selected[i].id).className = "letterCorrect";
                }
            }
        }
    }
    addListeners();
}

//letters listeners
function addListeners(){
    if (windowWidth <= 1280){
        for (let i = 0; i < letters.length; i++) {
            letters[i].addEventListener("click", selectLetter);
        }
    } else {
        for (let i = 0; i < letters.length; i++) {
            letters[i].addEventListener("click", selectLetter);
            letters[i].addEventListener("mouseover", hoveredLetter);
            letters[i].addEventListener("mouseout", leaveLetter);
        }
    }
}


//select initial or final letter
function selectLetter(){
    const noImage = imagesArray.find(function(el){
        return el.className === "imgSelected"
    });
    const imageSelected = imagesArray.find(function(el){
        return el.className === "imgSelected"
    });
    const allImages = imagesArray.every(function(el){
        return el.className === "imageCorrect";
    });
    noneSelected = lettersArray.every(function(el){
        return el.className === "";
    });
    manySelected = lettersArray.filter(function(el){
        return el.className === "letterSelected" || el.className === "letterCorrectRed";
    });
    someCorrect = lettersArray.filter(function(el){
        return el.className === "letterCorrect";
    });
    onlySelected = lettersArray.filter(function(el){
        return el.className === "letterSelected";
    });
    let wrongLettersA, wrongLettersB;

    let wordSelection = "";

    if (windowWidth <= 1280){
        if (noImage !== undefined){
            if (firstLetter === ""){
                this.className = "letterSelected";
                firstLetter = Number.parseInt(this.id.slice(3), 10);
            } else {
                if (Number.parseInt(this.id.slice(3), 10) === firstLetter){
                    this.className = "";
                    firstLetter = "";
                } else {
                    this.className = "letterSelected";
                    lastLetter = Number.parseInt(this.id.slice(3), 10);
                    for (let i = 0; i < letters.length; i++) {
                        letters[i].removeEventListener("click", selectLetter);
                    }
                }
            }

            const fruitImage = document.getElementById(imageSelected.id).getAttribute("value");
            if (firstLetter !== "" && lastLetter !== ""){
                const distance = lastLetter - firstLetter;
                if (distance % 100 === 0 && distance < 0){
                    //vertical up
                    for (let i = firstLetter; i >= lastLetter; i -= 100){
                        if (document.getElementById("box" + i).className === "letterCorrect"){
                            document.getElementById("box" + i).className = "letterCorrectRed";
                        } else {
                            document.getElementById("box" + i).className = "letterSelected";
                        }
                        wordSelection += document.getElementById("box" + i).innerHTML;
                    }
                    if (wordSelection.toLowerCase() === fruitImage || wordSelection.toLowerCase().split("").reverse().join("") === fruitImage){
                        document.getElementById(imageSelected.id).className = "imageCorrect";
                        for (let i = firstLetter; i >= lastLetter +1; i -= 100){
                            document.getElementById("box" + i).className = "letterCorrect";
                        }

                        let imagesDone = document.getElementsByClassName("imageCorrect").length;
                        if (imagesDone === 10){
                           setTimeout(() => (endingModal.style.display = "block"), 500); 
                        }

                    } else {
                        document.getElementById(imageSelected.id).className = "imageIncorrect";
                        setTimeout(() => (document.getElementById(imageSelected.id).className = "image"), 500);
                        for (let i = firstLetter; i >= lastLetter +1; i -= 100){
                            if (document.getElementById("box" + i).className === "letterCorrectRed"){
                                document.getElementById("box" + i).className = "letterCorrectGrey";
                                setTimeout(() => (document.getElementById("box" + i).className = "letterCorrect"), 500);
                            } else {
                                document.getElementById("box" + i).className = "letterIncorrect";
                                setTimeout(() => (document.getElementById("box" + i).className = ""), 500);
                            }
                        }
                    }
                } else if (distance % 100 === 0 && distance > 0){
                    //vertical down
                    for (let i = firstLetter; i <= lastLetter; i += 100){
                        if (document.getElementById("box" + i).className === "letterCorrect"){
                            document.getElementById("box" + i).className = "letterCorrectRed";
                        } else {
                            document.getElementById("box" + i).className = "letterSelected";
                        }
                        wordSelection += document.getElementById("box" + i).innerHTML;
                    }
                    if (wordSelection.toLowerCase() === fruitImage || wordSelection.toLowerCase().split("").reverse().join("") === fruitImage){
                        document.getElementById(imageSelected.id).className = "imageCorrect";
                        for (let i = firstLetter; i <= lastLetter; i += 100){
                            document.getElementById("box" + i).className = "letterCorrect";
                        }

                        let imagesDone = document.getElementsByClassName("imageCorrect").length;
                        if (imagesDone === 10){
                           setTimeout(() => (endingModal.style.display = "block"), 500); 
                        }

                    } else {
                        document.getElementById(imageSelected.id).className = "imageIncorrect";
                        setTimeout(() => (document.getElementById(imageSelected.id).className = "image"), 500);
                        for (let i = firstLetter; i <= lastLetter; i += 100){
                            if (document.getElementById("box" + i).className === "letterCorrectRed"){
                                document.getElementById("box" + i).className = "letterCorrectGrey";
                                setTimeout(() => (document.getElementById("box" + i).className = "letterCorrect"), 500);
                            } else {
                                document.getElementById("box" + i).className = "letterIncorrect";
                                setTimeout(() => (document.getElementById("box" + i).className = ""), 500);
                            }
                        }
                    }
                } else if (distance % 99 === 0 && distance < 0){
                    //diagonal up right
                    for (let i = firstLetter; i >= lastLetter; i -= 99){
                        if (document.getElementById("box" + i).className === "letterCorrect"){
                            document.getElementById("box" + i).className = "letterCorrectRed";
                        } else {
                            document.getElementById("box" + i).className = "letterSelected";
                        }
                        wordSelection += document.getElementById("box" + i).innerHTML;
                    }
                    if (wordSelection.toLowerCase() === fruitImage || wordSelection.toLowerCase().split("").reverse().join("") === fruitImage){
                        document.getElementById(imageSelected.id).className = "imageCorrect";
                        for (let i = firstLetter; i >= lastLetter; i -= 99){
                            document.getElementById("box" + i).className = "letterCorrect";
                        }

                        let imagesDone = document.getElementsByClassName("imageCorrect").length;
                        if (imagesDone === 10){
                           setTimeout(() => (endingModal.style.display = "block"), 500); 
                        }

                    } else {
                        document.getElementById(imageSelected.id).className = "imageIncorrect";
                        setTimeout(() => (document.getElementById(imageSelected.id).className = "image"), 500);
                        for (let i = firstLetter; i >= lastLetter; i -= 99){
                            if (document.getElementById("box" + i).className === "letterCorrectRed"){
                                document.getElementById("box" + i).className = "letterCorrectGrey";
                                setTimeout(() => (document.getElementById("box" + i).className = "letterCorrect"), 500);
                            } else {
                                document.getElementById("box" + i).className = "letterIncorrect";
                                setTimeout(() => (document.getElementById("box" + i).className = ""), 500);
                            }
                        }
                    }
                } else if (distance % 99 === 0 && distance > 0){
                    //diagonal down left
                    for (let i = firstLetter; i <= lastLetter; i += 99){
                        if (document.getElementById("box" + i).className === "letterCorrect"){
                            document.getElementById("box" + i).className = "letterCorrectRed";
                        } else {
                            document.getElementById("box" + i).className = "letterSelected";
                        }
                        wordSelection += document.getElementById("box" + i).innerHTML;
                    }
                    if (wordSelection.toLowerCase() === fruitImage || wordSelection.toLowerCase().split("").reverse().join("") === fruitImage){
                        document.getElementById(imageSelected.id).className = "imageCorrect";
                        for (let i = firstLetter; i <= lastLetter; i += 99){
                            document.getElementById("box" + i).className = "letterCorrect";
                        }

                        let imagesDone = document.getElementsByClassName("imageCorrect").length;
                        if (imagesDone === 10){
                           setTimeout(() => (endingModal.style.display = "block"), 500); 
                        }

                    } else {
                        document.getElementById(imageSelected.id).className = "imageIncorrect";
                        setTimeout(() => (document.getElementById(imageSelected.id).className = "image"), 500);
                        for (let i = firstLetter; i <= lastLetter; i += 99){
                            if (document.getElementById("box" + i).className === "letterCorrectRed"){
                                document.getElementById("box" + i).className = "letterCorrectGrey";
                                setTimeout(() => (document.getElementById("box" + i).className = "letterCorrect"), 500);
                            } else {
                                document.getElementById("box" + i).className = "letterIncorrect";
                                setTimeout(() => (document.getElementById("box" + i).className = ""), 500);
                            }
                        }
                    }
                } else if (distance % 101 === 0 && distance < 0){
                    //diagonal up left
                    for (let i = firstLetter; i >= lastLetter; i -= 101){
                        if (document.getElementById("box" + i).className === "letterCorrect"){
                            document.getElementById("box" + i).className = "letterCorrectRed";
                        } else {
                            document.getElementById("box" + i).className = "letterSelected";
                        }
                        wordSelection += document.getElementById("box" + i).innerHTML;
                    }
                    if (wordSelection.toLowerCase() === fruitImage || wordSelection.toLowerCase().split("").reverse().join("") === fruitImage){
                        document.getElementById(imageSelected.id).className = "imageCorrect";
                        for (let i = firstLetter; i >= lastLetter; i -= 101){
                            document.getElementById("box" + i).className = "letterCorrect";
                        }

                        let imagesDone = document.getElementsByClassName("imageCorrect").length;
                        if (imagesDone === 10){
                           setTimeout(() => (endingModal.style.display = "block"), 500); 
                        }

                    } else {
                        document.getElementById(imageSelected.id).className = "imageIncorrect";
                        setTimeout(() => (document.getElementById(imageSelected.id).className = "image"), 500);
                        for (let i = firstLetter; i >= lastLetter; i -= 101){
                            if (document.getElementById("box" + i).className === "letterCorrectRed"){
                                document.getElementById("box" + i).className = "letterCorrectGrey";
                                setTimeout(() => (document.getElementById("box" + i).className = "letterCorrect"), 500);
                            } else {
                                document.getElementById("box" + i).className = "letterIncorrect";
                                setTimeout(() => (document.getElementById("box" + i).className = ""), 500);
                            }
                        }
                    }
                } else if (distance % 101 === 0 && distance > 0){
                    //diagonal down right
                    for (let i = firstLetter; i <= lastLetter; i += 101){
                        if (document.getElementById("box" + i).className === "letterCorrect"){
                            document.getElementById("box" + i).className = "letterCorrectRed";
                        } else {
                            document.getElementById("box" + i).className = "letterSelected";
                        }
                        wordSelection += document.getElementById("box" + i).innerHTML;
                    }
                    if (wordSelection.toLowerCase() === fruitImage || wordSelection.toLowerCase().split("").reverse().join("") === fruitImage){
                        document.getElementById(imageSelected.id).className = "imageCorrect";
                        for (let i = firstLetter; i <= lastLetter; i += 101){
                            document.getElementById("box" + i).className = "letterCorrect";
                        }

                        let imagesDone = document.getElementsByClassName("imageCorrect").length;
                        if (imagesDone === 10){
                           setTimeout(() => (endingModal.style.display = "block"), 500); 
                        }

                    } else {
                        document.getElementById(imageSelected.id).className = "imageIncorrect";
                        setTimeout(() => (document.getElementById(imageSelected.id).className = "image"), 500);
                        for (let i = firstLetter; i <= lastLetter; i += 101){
                            if (document.getElementById("box" + i).className === "letterCorrectRed"){
                                document.getElementById("box" + i).className = "letterCorrectGrey";
                                setTimeout(() => (document.getElementById("box" + i).className = "letterCorrect"), 500);
                            } else {
                                document.getElementById("box" + i).className = "letterIncorrect";
                                setTimeout(() => (document.getElementById("box" + i).className = ""), 500);
                            }
                        }
                    }
                } else if (firstLetter.toString().length === 3 && lastLetter.toString().length === 3 && firstLetter.toString().slice(0, 1) === lastLetter.toString().slice(0, 1)){
                    if (firstLetter < lastLetter){
                        //towards right
                        for (let i = firstLetter; i <= lastLetter; i++){
                            if (document.getElementById("box" + i).className === "letterCorrect"){
                                document.getElementById("box" + i).className = "letterCorrectRed";
                            } else {
                                document.getElementById("box" + i).className = "letterSelected";
                            }
                            wordSelection += document.getElementById("box" + i).innerHTML;
                        }
                        if (wordSelection.toLowerCase() === fruitImage || wordSelection.toLowerCase().split("").reverse().join("") === fruitImage){
                            document.getElementById(imageSelected.id).className = "imageCorrect";
                            for (let i = firstLetter; i <= lastLetter; i++){
                                document.getElementById("box" + i).className = "letterCorrect";
                            }

                        let imagesDone = document.getElementsByClassName("imageCorrect").length;
                        if (imagesDone === 10){
                           setTimeout(() => (endingModal.style.display = "block"), 500); 
                        }

                        } else {
                            document.getElementById(imageSelected.id).className = "imageIncorrect";
                            setTimeout(() => (document.getElementById(imageSelected.id).className = "image"), 500);
                            for (let i = firstLetter; i <= lastLetter; i++){
                                if (document.getElementById("box" + i).className === "letterCorrectRed"){
                                    document.getElementById("box" + i).className = "letterCorrectGrey";
                                    setTimeout(() => (document.getElementById("box" + i).className = "letterCorrect"), 500);
                                } else {
                                    document.getElementById("box" + i).className = "letterIncorrect";
                                    setTimeout(() => (document.getElementById("box" + i).className = ""), 500);
                                }
                            }
                        }
                    } else if (firstLetter > lastLetter){
                        //towards left
                        for (let i = firstLetter; i >= lastLetter; i--){
                            if (document.getElementById("box" + i).className === "letterCorrect"){
                                document.getElementById("box" + i).className = "letterCorrectRed";
                            } else {
                                document.getElementById("box" + i).className = "letterSelected";
                            }
                            wordSelection += document.getElementById("box" + i).innerHTML;
                        }
                        if (wordSelection.toLowerCase() === fruitImage || wordSelection.toLowerCase().split("").reverse().join("") === fruitImage){
                            document.getElementById(imageSelected.id).className = "imageCorrect";
                            for (let i = firstLetter; i >= lastLetter; i--){
                                document.getElementById("box" + i).className = "letterCorrect";
                            }

                        let imagesDone = document.getElementsByClassName("imageCorrect").length;
                        if (imagesDone === 10){
                           setTimeout(() => (endingModal.style.display = "block"), 500); 
                        }

                        } else {
                            document.getElementById(imageSelected.id).className = "imageIncorrect";
                            setTimeout(() => (document.getElementById(imageSelected.id).className = "image"), 500);
                            for (let i = firstLetter; i >= lastLetter; i--){
                                if (document.getElementById("box" + i).className === "letterCorrectRed"){
                                    document.getElementById("box" + i).className = "letterCorrectGrey";
                                    setTimeout(() => (document.getElementById("box" + i).className = "letterCorrect"), 500);
                                } else {
                                    document.getElementById("box" + i).className = "letterIncorrect";
                                    setTimeout(() => (document.getElementById("box" + i).className = ""), 500);
                                }
                            }
                        }
                    }
                } else if (firstLetter.toString().length === 4 && lastLetter.toString().length === 4 && firstLetter.toString().slice(0, 2) === lastLetter.toString().slice(0, 2)){
                    if (firstLetter < lastLetter){
                        //towards right
                        for (let i = firstLetter; i <= lastLetter; i++){
                            if (document.getElementById("box" + i).className === "letterCorrect"){
                                document.getElementById("box" + i).className = "letterCorrectRed";
                            } else {
                                document.getElementById("box" + i).className = "letterSelected";
                            }
                            wordSelection += document.getElementById("box" + i).innerHTML;
                        }
                        if (wordSelection.toLowerCase() === fruitImage || wordSelection.toLowerCase().split("").reverse().join("") === fruitImage){
                            document.getElementById(imageSelected.id).className = "imageCorrect";
                            for (let i = firstLetter; i <= lastLetter; i++){
                                document.getElementById("box" + i).className = "letterCorrect";
                            }

                        let imagesDone = document.getElementsByClassName("imageCorrect").length;
                        if (imagesDone === 10){
                           setTimeout(() => (endingModal.style.display = "block"), 500); 
                        }

                        } else {
                            document.getElementById(imageSelected.id).className = "imageIncorrect";
                            setTimeout(() => (document.getElementById(imageSelected.id).className = "image"), 500);
                            for (let i = firstLetter; i <= lastLetter; i++){
                                if (document.getElementById("box" + i).className === "letterCorrectRed"){
                                    document.getElementById("box" + i).className = "letterCorrectGrey";
                                    setTimeout(() => (document.getElementById("box" + i).className = "letterCorrect"), 500);
                                } else {
                                    document.getElementById("box" + i).className = "letterIncorrect";
                                    setTimeout(() => (document.getElementById("box" + i).className = ""), 500);
                                }
                            }
                        }
                    } else if (firstLetter > lastLetter){
                        //towards left
                        for (let i = firstLetter; i >= lastLetter; i--){
                            if (document.getElementById("box" + i).className === "letterCorrect"){
                                document.getElementById("box" + i).className = "letterCorrectRed";
                            } else {
                                document.getElementById("box" + i).className = "letterSelected";
                            }
                            wordSelection += document.getElementById("box" + i).innerHTML;
                        }
                        if (wordSelection.toLowerCase() === fruitImage || wordSelection.toLowerCase().split("").reverse().join("") === fruitImage){
                            document.getElementById(imageSelected.id).className = "imageCorrect";
                            for (let i = firstLetter; i >= lastLetter; i--){
                                document.getElementById("box" + i).className = "letterCorrect";
                            }

                        let imagesDone = document.getElementsByClassName("imageCorrect").length;
                        if (imagesDone === 10){
                           setTimeout(() => (endingModal.style.display = "block"), 500); 
                        }

                        } else {
                            document.getElementById(imageSelected.id).className = "imageIncorrect";
                            setTimeout(() => (document.getElementById(imageSelected.id).className = "image"), 500);
                            for (let i = firstLetter; i >= lastLetter; i--){
                                if (document.getElementById("box" + i).className === "letterCorrectRed"){
                                    document.getElementById("box" + i).className = "letterCorrectGrey";
                                    setTimeout(() => (document.getElementById("box" + i).className = "letterCorrect"), 500);
                                } else {
                                    document.getElementById("box" + i).className = "letterIncorrect";
                                    setTimeout(() => (document.getElementById("box" + i).className = ""), 500);
                                }
                            }
                        }
                    }
 
                } else {
                    firstLetter.className = "";
                    lastLetter.className = "";
                    document.getElementById(imageSelected.id).className = "imageIncorrect";
                    setTimeout(() => (document.getElementById(imageSelected.id).className = "image"), 500);
                }
                firstLetter = "";
                lastLetter = "";
            }
        }
    } else {
        if (noImage !== undefined) {
            if (noneSelected === true) {
                this.className = "letterSelected";
                initial = this.id;
            } else if (noneSelected === false && someCorrect === undefined){
                this.className = "";
                intial = "";
            } else if (noneSelected === false && someCorrect !== undefined && onlySelected.length === 0){
                if (this.className === "") {
                    this.className = "letterSelected";
                } else if (this.className === "letterCorrect"){
                    this.className = "letterCorrectRed";
                }
                initial = this.id;
            } else {
                if (manySelected.length === 1) {
                    if (this.className === "letterSelected") {
                        this.className = "";
                    } else if (this.className === "letterCorrectRed"){
                        this.className = "letterCorrect";
                    }
                    initial = "";
                } else if (manySelected.length > 1) {
                    const fruitImage = document.getElementById(imageSelected.id).getAttribute("value");
                    let fruitName = "";
                    for (let k = 0; k < manySelected.length; k++){
                        const letter = document.getElementById(manySelected[k].id).innerHTML;
                        fruitName += letter;
                    }

                    if (fruitImage == fruitName.toLowerCase() || fruitImage == fruitName.toLowerCase().split("").reverse().join("")){
                        document.getElementById(imageSelected.id).className = "imageCorrect";
                        for (let k = 0; k < manySelected.length; k++) {
                            document.getElementById(manySelected[k].id).className = "letterCorrect";
                        }
                        initial = "";

                        let imagesDone = document.getElementsByClassName("imageCorrect").length;
                        if (imagesDone === 10){
                           setTimeout(() => (endingModal.style.display = "block"), 500); 
                        }

                    } else {
                        document.getElementById(imageSelected.id).className = "imageIncorrect";
                        setTimeout(() => (document.getElementById(imageSelected.id).className = "image"), 500);
                        for (let k = 0; k < manySelected.length; k++) {
                            if (document.getElementById(manySelected[k].id).className === "letterSelected"){ 
                                document.getElementById(manySelected[k].id).className = "letterIncorrect";
                                wrongLettersA = lettersArray.filter(function(el){
                                    return el.className === "letterIncorrect"});
                                for (let m = 0; m < wrongLettersA.length; m++){
                                        setTimeout(() => (document.getElementById(wrongLettersA[m].id).className = ""), 500);
                                }
                            } else if (document.getElementById(manySelected[k].id).className === "letterCorrectRed"){
                                document.getElementById(manySelected[k].id).className = "letterCorrectGrey";
                                wrongLettersB = lettersArray.filter(function(el){
                                    return el.className === "letterCorrectGrey"});
                                for (let m = 0; m < wrongLettersB.length; m++){
                                        setTimeout(() => (document.getElementById(wrongLettersB[m].id).className = "letterCorrect"), 500);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

//with letter selected, expand word
function hoveredLetter() {
    noneSelected = lettersArray.every(function(el){
        return el.className === "";
    });
    someCorrect = lettersArray.filter(function(el){
        return el.className === "letterCorrect";
    });
    manySelected = lettersArray.filter(function(el){
        return el.className === "letterSelected" || el.className === "letterCorrectRed";
    });
    onlySelected = lettersArray.filter(function(el){
        return el.className === "letterSelected";
    });
    onlyCorrect = lettersArray.filter(function(el){
        return el.className === "letterCorrectRed";
    });

    if (noneSelected === false){
        if ((document.getElementById(initial).className === "letterSelected") && (manySelected.length === 1) ||
            (document.getElementById(initial).className === "letterCorrectRed") && (manySelected.length === 1)){
                
                const letterNum = initial.slice(3);
                const boxesAround = [("box" + (+letterNum - 1)), ("box" + (+letterNum + 1)), ("box" + (+letterNum - 100)), ("box" + (+letterNum + 100)),
                    ("box" + (+letterNum - 101)), ("box" + (+letterNum - 99)), ("box" + (+letterNum + 99)), ("box" + (+letterNum + 101))];
                if (boxesAround.includes(this.id) === true){
                    if (this.className === "") {
                        this.className = "letterSelected"; 
                    } else if (this.className === "letterCorrect") {
                        this.className = "letterCorrectRed"; 
                    }
                }
        } else if ((document.getElementById(initial).className === "letterSelected") && (manySelected.length > 1) ||
            (document.getElementById(initial).className === "letterCorrectRed") && (manySelected.length > 1)){
                if (manySelected[0].id === initial){
                    const difference = (manySelected[1].id.slice(3)) - (manySelected[0].id.slice(3));
                    if (difference === 1){
                        for (let i = 1; i < (15 - manySelected[1].id.slice(-2)); i++){
                            if(this.id.slice(3) == (+manySelected[1].id.slice(3) + 1*(+i))){
                                if (this.className === "") {
                                    this.className = "letterSelected"; 
                                } else if (this.className === "letterCorrect") {
                                    this.className = "letterCorrectRed"; 
                                }
                            }
                        }
                    } else if (difference === 99) {
                        for (let i = 1; i < (manySelected[1].id.slice(-2)); i++){
                            if(this.id.slice(3) == (+manySelected[1].id.slice(3) + 99*(+i))){
                                if (this.className === "") {
                                    this.className = "letterSelected"; 
                                } else if (this.className === "letterCorrect") {
                                    this.className = "letterCorrectRed"; 
                                }
                            }
                        }
                    } else if (difference === 100) {
                        for (let i = 1; i < (15 - manySelected[1].id.slice(3,-2)); i++){
                            if(this.id.slice(3) == (+manySelected[1].id.slice(3) + 100*(+i))){
                                if (this.className === "") {
                                    this.className = "letterSelected"; 
                                } else if (this.className === "letterCorrect") {
                                    this.className = "letterCorrectRed"; 
                                }
                            }
                        }
                    } else if (difference === 101){
                        for (let i = 1; i < (15 - manySelected[1].id.slice(-2)); i++){
                            if(this.id.slice(3) == (+manySelected[1].id.slice(3) + 101*(+i))){
                                if (this.className === "") {
                                    this.className = "letterSelected"; 
                                } else if (this.className === "letterCorrect") {
                                    this.className = "letterCorrectRed"; 
                                }
                            }
                        }
                    }
                } else {
                    const difference = (manySelected[0].id.slice(3)) - (manySelected[1].id.slice(3));
                    if (difference === -1){
                        for (let i = 1; i < (manySelected[0].id.slice(-2)); i++){
                            if(this.id.slice(3) == (+manySelected[0].id.slice(3) - 1*(+i))){
                                if (this.className === "") {
                                    this.className = "letterSelected"; 
                                } else if (this.className === "letterCorrect") {
                                    this.className = "letterCorrectRed"; 
                                };
                            }
                        }
                    } else if (difference === -99) {
                        for (let i = 1; i < (15 - manySelected[0].id.slice(-2)); i++){
                            if(this.id.slice(3) == (+manySelected[0].id.slice(3) - 99*(+i))){
                                if (this.className === "") {
                                    this.className = "letterSelected"; 
                                } else if (this.className === "letterCorrect") {
                                    this.className = "letterCorrectRed"; 
                                }
                            }
                        }
                    } else if (difference === -100) {
                        for (let i = 1; i < (manySelected[0].id.slice(3,-2) +1); i++){
                            if(this.id.slice(3) == (+manySelected[1].id.slice(3) - 100*(+i))){
                                if (this.className === "") {
                                    this.className = "letterSelected"; 
                                } else if (this.className === "letterCorrect") {
                                    this.className = "letterCorrectRed"; 
                                }
                            }
                        }
                    } else if (difference === -101){
                        for (let i = 1; i < (manySelected[0].id.slice(-2)); i++){
                            if(this.id.slice(3) == (+manySelected[0].id.slice(3) - 101*(+i))){
                                if (this.className === "") {
                                    this.className = "letterSelected"; 
                                } else if (this.className === "letterCorrect") {
                                    this.className = "letterCorrectRed"; 
                                }
                            }
                        }
                    }
                }
        }
    }    
}

//with letter selected, return on your steps; 
function leaveLetter (event) {
    noneSelected = lettersArray.every(function(el){
        return el.className === "";
    });
    someCorrect = lettersArray.filter(function(el){
        return el.className === "letterCorrect";
    });
    manySelected = lettersArray.filter(function(el){
        return el.className === "letterSelected" || el.className === "letterCorrectRed";
    });
    onlySelected = lettersArray.filter(function(el){
        return el.className === "letterSelected";
    });
    onlyCorrect = lettersArray.filter(function(el){
        return el.className === "letterCorrectRed";
    });

    if (noneSelected === false) {
        if (manySelected.length === 2){
            let related = event.relatedTarget;
            const letterNum = initial.slice(3);
            const boxesAround = [("box" + (+letterNum - 1)), ("box" + (+letterNum + 1)), ("box" + (+letterNum - 100)), ("box" + (+letterNum + 100)),
                ("box" + (+letterNum - 101)), ("box" + (+letterNum - 99)), ("box" + (+letterNum + 99)), ("box" + (+letterNum + 101))];
            if (boxesAround.includes(related.id) === true) {
                if (this.className === "letterSelected"){
                    this.className = "";
                } else if (this.className === "letterCorrectRed"){
                    this.className = "letterCorrect";
                }
            } else if (related.id === initial){
                if (this.className === "letterSelected"){
                    this.className = "";
                } else if (this.className === "letterCorrectRed"){
                    this.className = "letterCorrect";
                }
            }
        } else if (manySelected.length > 2){
            let related = event.relatedTarget.id.slice(3);
            if (manySelected[0].id === initial){
                const difference = (manySelected[1].id.slice(3)) - (manySelected[0].id.slice(3));
                if (difference === 1 && related - +this.id.slice(3) === -1){
                    if (this.className === "letterSelected"){
                        this.className = "";
                    } else if (this.className === "letterCorrectRed"){
                        this.className = "letterCorrect";
                    }
                } else if (difference === 99 && (related - +this.id.slice(3) === -99 || related - +this.id.slice(3) === -100 || related - +this.id.slice(3) === 1)){
                    if (this.className === "letterSelected"){
                        this.className = "";
                    } else if (this.className === "letterCorrectRed"){
                        this.className = "letterCorrect";
                    }
                }  else if (difference === 100 && related - +this.id.slice(3) === -100){
                    if (this.className === "letterSelected"){
                        this.className = "";
                    } else if (this.className === "letterCorrectRed"){
                        this.className = "letterCorrect";
                    }
                }  else if (difference === 101 && (related - +this.id.slice(3) === -101 || related - +this.id.slice(3) === -100 || related - +this.id.slice(3) === -1)){
                    if (this.className === "letterSelected"){
                        this.className = "";
                    } else if (this.className === "letterCorrectRed"){
                        this.className = "letterCorrect";
                    }
                }
            } else {
                const difference = (manySelected[manySelected.length - 2].id.slice(3)) - (manySelected[manySelected.length - 1].id.slice(3));
                if (difference === -1 && related - +this.id.slice(3) === 1){
                    if (this.className === "letterSelected"){
                        this.className = "";
                    } else if (this.className === "letterCorrectRed"){
                        this.className = "letterCorrect";
                    }
                } else if (difference === -99 && (related - +this.id.slice(3) === 99  || related - +this.id.slice(3) === 100 || related - +this.id.slice(3) === -1 )){
                    if (this.className === "letterSelected"){
                        this.className = "";
                    } else if (this.className === "letterCorrectRed"){
                        this.className = "letterCorrect";
                    }
                }  else if (difference === -100 && related - +this.id.slice(3) === 100){
                    if (this.className === "letterSelected"){
                        this.className = "";
                    } else if (this.className === "letterCorrectRed"){
                        this.className = "letterCorrect";
                    }
                }  else if (difference === -101 && (related - +this.id.slice(3) === 101 || related - +this.id.slice(3) === 100 || related - +this.id.slice(3) === 1)){
                    if (this.className === "letterSelected"){
                        this.className = "";
                    } else if (this.className === "letterCorrectRed"){
                        this.className = "letterCorrect";
                    }
                } 
            }    
        }
    }
}