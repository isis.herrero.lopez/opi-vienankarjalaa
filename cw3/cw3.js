//number of columns
const columns = 13;
//number of rows
const rows = 20;

//coordinates of boxes with letter or image in them
const fullBoxes = ["b108", "b208", "b210", "b301", "b302", "b303", "b304", "b305", "b306", "b307", "b308", "b309", "b310", "b311","b312", 
"b408", "b410", "b412", "b504", "b506", "b507", "b508", "b509", "b510", "b511", "b512", 
"b604", "b608", "b610", "b612", "b704", "b708", "b710", "b712", "b804", "b810", "b812", "b904", "b910", "b912",
"b1002", "b1003", "b1004", "b1005", "b1006", "b1007", "b1008", "b1009", "b1010", "b1011", "b1012", "b1013",
"b1104", "b1110", "b1112", "b1204", "b1304", "b1401", "b1402", "b1403", "b1404", "b1405", "b1406", "b1407", "b1408", "b1409", "b1410", 
"b1504", "b1603", "b1604", "b1605", "b1606", "b1607", "b1608", "b1704", "b1804", "b1904", "b2004"];

//array with the first boxes (which go with images);
const imageBoxes = ["b108", "b210", "b301", "b312", "b504", "b506", "b1002", "b1401", "b1603"];
//array with the answers (int he same order);
const answers = ["ratijo", "pakaissin", "jiäškuappi", "kaijutin","aštienpešukoneh", "kiukua", "vejenkeitin", "pešuallaš", "krana"];
// array with names of images (in the same order);
const images = ["ratijo", "pakaissin", "jiaskuappi", "kaijutin", "astienpesukoneh", "kiukua", "vejenkeitin", "pesuallas", "krana"];
//array of final box for the words
const lastBoxes = ["b311", "b512", "b708", "b1013", "b1110", "b1112", "b1410", "b1608", "b2004"];
//correpondence between last box and the first box (the one with the image);
const names = {
    b311: "b302",
    b512: "b507",
    b708: "b208",
    b1013: "b1003",
    b1110: "b310",
    b1112: "b412",
    b1410: "b1402",
    b1608: "b1604",
    b2004: "b604",
}
//array with the initial box of the words in vertical
const verticalOnes = ["b108", "b210", "b312", "b504"];


const mainBox = document.getElementById("mainBox");

window.addEventListener("load", createBoxes);

function createBoxes(){
    mainBox.style.gridTemplateColumns = "repeat(" + columns + ", calc(100%/" + columns +"))";
    mainBox.style.gridTemplateRows = "repeat(" + rows + ", 40px)";

    for (let i = 1; i <= rows; i++){
        for (let k = 1; k <= columns; k++){
            if (k < 10){
                mainBox.innerHTML += `<div class="letterBox" id="b${i}0${k}"></div>`;
            } else {
                mainBox.innerHTML += `<div class="letterBox" id="b${i}${k}"></div>`;
            }
        }
    }

    for(let i = 0; i < fullBoxes.length; i++){
        document.getElementById(fullBoxes[i]).className += " full";
    }

    for (let i = 0; i < imageBoxes.length; i++){
        document.getElementById(imageBoxes[i]).style.backgroundImage = "url('cw3 images/" + images[i] + ".png')";
        document.getElementById(imageBoxes[i]).style.backgroundColor = "white";
        document.getElementById(imageBoxes[i]).style.backgroundRepeat = "no-repeat";
        document.getElementById(imageBoxes[i]).style.backgroundPosition = "center";
        document.getElementById(imageBoxes[i]).style.backgroundSize = "contain";
        document.getElementById(imageBoxes[i]).addEventListener("click", activateWriting);
    }
 
    for (let i = 0; i < fullBoxes.length; i++){
        if (imageBoxes.includes(fullBoxes[i]) == false){
            document.getElementById(fullBoxes[i]).outerHTML = `<input class="letterBox full" id="${fullBoxes[i]}" disabled=true>`;  
        }
    }

    for (let i = 0; i < lastBoxes.length; i++){
        document.getElementById(lastBoxes[i]).setAttribute("name", names[lastBoxes[i]]);
    }
}

let clientY, myThis;

function activateWriting(ev){
    if (this.className === "letterBox full" && document.getElementsByClassName("letterBox full selected").length === 0){
        console.log("None selected -- I can select this one");
        this.className = "letterBox full selected";
        myThis = this;
        clientY = ev.clientY;

//make images larger
        imageLarger();

        imageId = this.id;
        box = Number.parseInt(imageId.slice(1), 10);
        if (verticalOnes.includes(imageId)){
            console.log("vertical");
            if (imageId.length === 4){
                lenght4V();
            } else if (imageId.length === 5){
                length5V();
            }
        } else {
            console.log("horizontal");
            if (imageId.length === 4){
                length4H();
            } else if (imageId.length === 5){
                length5H();
            }
        }
    } else if (this.className === "letterBox full" && document.getElementsByClassName("letterBox full selected").length === 1){
        console.log("already one selected -- change between images");
        selected = document.getElementsByClassName("letterBox full selected")[0].id;
        box = Number.parseInt(selected.slice(1), 10);
        if (verticalOnes.includes(selected)){
            if (selected.length === 4){
                empty4V();

                this.className = "letterBox full selected";
                myThis = this;
                clientY = ev.clientY;
                imageLarger();

                imageId = this.id;
                box = Number.parseInt(imageId.slice(1), 10);
                if (verticalOnes.includes(imageId)){
                    console.log("vertical");
                    if (imageId.length === 4){
                        lenght4V();
                    } else if (imageId.length === 5){
                        length5V();
                    }
                } else {
                    console.log("horizontal");
                    if (imageId.length === 4){
                        length4H();
                    } else if (imageId.length === 5){
                        length5H();
                    }
                }
            } else if (imageId.length === 5){
                empty5V();

                this.className = "letterBox full selected";
                myThis = this;
                clientY = ev.clientY;
                imageLarger();

                imageId = this.id;
                box = Number.parseInt(imageId.slice(1), 10);
                if (verticalOnes.includes(imageId)){
                    console.log("vertical");
                    if (imageId.length === 4){
                        lenght4V();
                    } else if (imageId.length === 5){
                        length5V();
                    }
                } else {
                    console.log("horizontal");
                    if (imageId.length === 4){
                        length4H();
                    } else if (imageId.length === 5){
                        length5H();
                    }
                }
            }
        } else {
            if (selected.length === 4){
                empty4H();

                this.className = "letterBox full selected";
                myThis = this;
                clientY = ev.clientY;
                imageLarger();

                imageId = this.id;
                box = Number.parseInt(imageId.slice(1), 10);
                if (verticalOnes.includes(imageId)){
                    console.log("vertical");
                    if (imageId.length === 4){
                        lenght4V();
                    } else if (imageId.length === 5){
                        length5V();
                   }
                } else {
                    console.log("horizontal");
                    if (imageId.length === 4){
                        length4H();
                    } else if (imageId.length === 5){
                        length5H();
                    }
                }
            } else if (selected.length === 5){
                empty5H();

                this.className = "letterBox full selected";
                myThis = this;
                clientY = ev.clientY;
                imageLarger();

                imageId = this.id;
                box = Number.parseInt(imageId.slice(1), 10);
                if (verticalOnes.includes(imageId)){
                    console.log("vertical");
                    if (imageId.length === 4){
                        lenght4V();
                    } else if (imageId.length === 5){
                        length5V();
                    }
                } else {
                    console.log("horizontal");
                    if (imageId.length === 4){
                        length4H();
                    } else if (imageId.length === 5){
                        length5H();
                    }
                }
            }
        }
    } else if (this.className === "letterBox full done") {
        console.log("nothing should happen; it's done!")
    }
}

function focusJumpV(){
    if (document.activeElement.value.length === document.activeElement.maxLength){
        boxNumber = Number.parseInt(document.activeElement.id.slice(1), 10);
        if (document.getElementById("b" + (boxNumber + 100) + "") == null || document.getElementById("b" + (boxNumber + 100) + "").tagName !== "INPUT"){
            console.log("last one");
            lastBox = Number.parseInt(this.id.slice(1), 10);
            initial = Number.parseInt(this.name.slice(1), 10);
            checkWordV();
        } else if (document.getElementById("b" + (boxNumber + 100) + "").className === "letterBox full correct"){
            if (document.getElementById("b" + (boxNumber + 200) + "").className === "letterBox full"){
                document.getElementById("b" + (boxNumber + 200) + "").focus();
            } else {
                console.log("last one is already correct");
                lastBox = boxNumber + 100;
                initial = Number.parseInt(document.getElementById("b" + (boxNumber + 100) + "").name);
                checkWordV();
            }
        } else {
            if (document.getElementById("b" + (boxNumber + 100) + "").className === "letterBox full correct"){
                document.getElementById("b" + (boxNumber + 200) + "").focus();
            } else {
                document.getElementById("b" + (boxNumber + 100) + "").focus();
            }
        }
    }
}

function focusJumpH(){
    if (document.activeElement.value.length === document.activeElement.maxLength){
        boxNumber = Number.parseInt(document.activeElement.id.slice(1), 10);
        if (document.getElementById("b" + (boxNumber + 1) + "") == null || document.getElementById("b" + (boxNumber + 1) + "").tagName !== "INPUT"){
            console.log("last one");
            lastBox = Number.parseInt(this.id.slice(1), 10);
            initial = Number.parseInt(this.name.slice(1), 10);
            checkWordH();
        } else if (document.getElementById("b" + (boxNumber + 1) + "").className === "letterBox full correct"){
            if (document.getElementById("b" + (boxNumber + 2) + "").className === "letterBox full"){
                document.getElementById("b" + (boxNumber + 2) + "").focus();
            } else {
                console.log("last one is already correct");
                lastBox = boxNumber + 1;
                initial = Number.parseInt(document.getElementById("b" + (boxNumber + 1) + "").name.slice(1), 10);
                checkWordH();
            }
        } else {
            if (document.getElementById("b" + (boxNumber + 1) + "").className === "letterBox full correct"){
                document.getElementById("b" + (boxNumber + 2) + "").focus();
            } else {
                document.getElementById("b" + (boxNumber + 1) + "").focus();
            }
        }
    }
}

let activeBox;
function getPreviousActive(){
    activeBox = document.activeElement.id;
}

function addLetter(id){
    const letter = id;
    document.getElementById(activeBox).value += letter;
    const activeNumber = Number.parseInt(activeBox.slice(1), 10);
    if (document.getElementById("b" + (activeNumber + 100) + "").className === "letterBox full" || document.getElementById("b" + (activeNumber - 100) + "").className === "letterBox full"){
        if (document.getElementById("b" + (activeNumber + 100) + "").tagName !== "INPUT"){
            boxNumber = activeNumber;
            lastBox = activeNumber;
            initial = Number.parseInt(document.getElementById("b" + boxNumber + "").name.slice(1), 10);
            checkWordV();
        } else if (document.getElementById("b" + (activeNumber + 100) + "").tagName === "INPUT" && document.getElementById("b" + (activeNumber + 100) + "").hasAttribute("disabled") === false){
            document.getElementById("b" + (activeNumber + 100) + "").focus();
        }
    } else if (document.getElementById("b" + (activeNumber + 1) + "").className === "letterBox full" || document.getElementById("b" + (activeNumber - 1) + "").className === "letterBox full"){
        if (document.getElementById("b" + (activeNumber + 1) + "").tagName !== "INPUT"){
            boxNumber = activeNumber;
            lastBox = activeNumber;
            initial = Number.parseInt(document.getElementById("b" + boxNumber + "").name.slice(1), 10);
            checkWordH();
        }  else if (document.getElementById("b" + (activeNumber + 1) + "").tagName === "INPUT" && document.getElementById("b" + (activeNumber + 1) + "").hasAttribute("disabled") === false){
            document.getElementById("b" + (activeNumber + 1) + "").focus();
        }
    }
}

function checkWordV(){
    let word = "";
    for (let i = initial; i <= lastBox; i = i + 100){
        word += document.getElementById("b" + i + "").value;
    }
    let index = imageBoxes.indexOf("b" + (initial - 100) + "");
    const rightAnswer = answers[index];
    if (word === rightAnswer || word.toLowerCase() === rightAnswer){
        console.log("right!");
        for (let i = initial; i <= lastBox; i = i + 100){
            if (document.getElementById("b" + i + "").className !== "letterBox full correct"){
                document.getElementById("b" + i + "").className += " correct";
                document.getElementById("b" + i + "").setAttribute("disabled", true);
            } else {
                console.log("already correct");
            }
        }
        document.getElementById("b" + (initial - 100) + "").className = "letterBox full done";
    } else {
        console.log("wrong!");
        for (let i = initial; i <= lastBox; i = i + 100){
            if (document.getElementById("b" + i + "").className === "letterBox full correct"){
                document.getElementById("b" + i + "").className += " wrong";
                setTimeout(() => (document.getElementById("b" + i + "").className = "letterBox full correct"), 500);
            } else {
            document.getElementById("b" + i + "").className += " wrong";
            document.getElementById("b" + i + "").setAttribute("disabled", true);
            setTimeout(() => (document.getElementById("b" + i + "").className = "letterBox full"), 500);
            setTimeout(() => (document.getElementById("b" + i + "").value = ""), 500);
            }
        }
        document.getElementById("b" + (initial - 100) + "").className = "letterBox full";
    }
}

function checkWordH(){
    console.log(lastBox + ", " + initial);
    let word = "";
    for (let i = initial; i <= lastBox; i++){
        word += document.getElementById("b" + i + "").value;
    }
    let index = imageBoxes.indexOf("b" + (initial - 1) + "");
    const rightAnswer = answers[index];
    if (word === rightAnswer || word.toLowerCase() === rightAnswer) {
        console.log("right!!");
        for (let i = initial; i <= lastBox; i++){
            if (document.getElementById("b" + i + "").className !== "letterBox full correct"){
                document.getElementById("b" + i + "").className += " correct";
                document.getElementById("b" + i + "").setAttribute("disabled", true);
            } else {
                console.log("already correct");
            }
        }
        document.getElementById("b" + (initial - 1) + "").className = "letterBox full done";
    } else {
        console.log("wrong!");
        for (let i = initial; i <= lastBox; i++){
            if (document.getElementById("b" + i + "").className === "letterBox full correct"){
                document.getElementById("b" + i + "").className += " wrong";
                setTimeout(() => (document.getElementById("b" + i + "").className = "letterBox full correct"), 500);
            } else {
            document.getElementById("b" + i + "").className += " wrong";
            document.getElementById("b" + i + "").setAttribute("disabled", true);
            setTimeout(() => (document.getElementById("b" + i + "").className = "letterBox full"), 500);
            setTimeout(() => (document.getElementById("b" + i + "").value = ""), 500);
            }
        }
        document.getElementById("b" + (initial - 1) + "").className = "letterBox full";
    }
}
